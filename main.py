#!/usr/bin/env python3
"""
#### Onze Bot -- A discord bot for "Onze server"
#### main.py
#### Copyright (C) PSJ Sokolov 2019
####
#### This program is free software: you can redistribute it and/or modify
#### it under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or
#### (at your option) any later version.
####
#### This program is distributed in the hope that it will be useful,
#### but WITHOUT ANY WARRANTY; without even the implied warranty of
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#### GNU Affero General Public License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <http://www.gnu.org/licenses/ >.
"""
### STANDARD LIBRARY IMPORTS:
import types
import signal
import sys

### FOREIGN IMPORTS:
## Discord
import discord

## nltk -- natural language toolkit
import nltk

## fuzzywuzzy -- a library for fuzzy matching, using Levenschtein-Distance
import fuzzywuzzy.process

## LOCAL IMPORTS:
import authentication_token
import commands

class MyClient(discord.Client):
    def __init__(self, command_table: dict, prefix: str = ';'):
        super().__init__()
        self.prefix = prefix
        self.command_table = command_table

    def handle_command(self, arguments: list):
        try:
            return self.command_table[arguments[1]](arguments[1:])
        except KeyError:
            suggestions = fuzzywuzzy.process.extract(arguments[1],self.command_table.keys())
            newline = '\n'
            index = 1
            suggestions_string = ""
            suggestions = filter(lambda x: x[1] > 50, suggestions)
            for suggestion in suggestions:
                suggestions_string += f"```{index}. {suggestion[0]} -- {self.command_table[suggestion[0]].__doc__.split(newline)[0]} ({suggestion[1]}% sure)```"
                index += 1
            return f"Sorry, I do not know the command {arguments[1]}, maybe you meant one of the following:" + "\n" + suggestions_string

    async def on_ready(self):
        print(f'Logged on as {self.user}')

    async def on_message(self, message):
        print(f'Message from {message.author}: {message.content}')
        tokens = nltk.word_tokenize(message.content)
        if tokens[0] == self.prefix:
            response = self.handle_command(tokens)
            if response:
                await message.channel.send(response)
        else:
            pass

if __name__ == '__main__':
    ## Built the commands dictionary:
    #  First extract the member functions from the command module.
    command_functions = [getattr(commands, function) for function in dir(commands)
                             if isinstance(getattr(commands, function), types.FunctionType)]
    #  Second put them and their attributes into a dictionary.
    command_table  = {com.__name__ : com for com in command_functions}

    # Create the client:
    client = MyClient(command_table)

    # Run the client
    client.run(authentication_token.token)
