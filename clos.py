def outer(init = 0):
    """make a closure"""
    def incrementer(increment = 1):
        """increment init by increment"""
        nonlocal init
        init += increment
    def printer():
        """print the state closed over"""
        print('init is: ', init)
    return {'print' : printer, 'inc' : incrementer}
