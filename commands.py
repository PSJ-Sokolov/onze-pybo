""" Commands.py -- Put bot commands in here.
"""
## SYSTEM IMPORTS:
import sys
import types

## Local state:
keren_gepest    = 0
pispaal_mention = '@PSJ#1461'

def ping(args: list) -> str:
    """pong -- return 'pong'"""
    return 'pong'

def echo(args: list):
    """echo [args] -- return args"""
    return ' '.join(args[1:])

def pestgedrag(args: list) -> str:
    """pestgedrag -- Laat zien hoe vaak iemand gepest is."""
    return f"{pispaal_mention} is {keren_gepest} keren gepest!"

def pispaal(mention: str) -> str:
    """pispaal MENTION -- begin MENTION te pesten."""
    global pispaal_mention # Tja dit moet in python, opzich wel goed,
    global keren_gepest    # maar de PHP oplossing was waarschijnlijk beter geweest.
    pispaal_mention = mention
    keren_gepest    = 0
    return f"Ok, {pispaal_mention} gaat nu gepest worden!"

def retard(args):
    """retard -- pest de huidigge pispaal"""
    global keren_gepest
    keren_gepest += 1

def help(args: list) -> str:
    """help COMMAND_NAME -- Show a commands documentation"""
    command_name = args[1]
    try:
        return command_table[command_name].__doc__
    except KeyError:
        return f"COULD NOT FIND HELP FOR THE COMMAND '{command_name}'"

## Bouw het hulpboek:
module  = sys.modules[__name__]
print(module)
print(dir(module))
print(list(filter(lambda x: isinstance(x, types.FunctionType), dir(module))))
command_functions = [
    getattr(module, function) for function in dir(module)
            if isinstance(getattr(module, function), types.FunctionType)
]
print(command_functions)
command_table     = {com.__name__ : com for com in command_functions}
print(command_table)